$(document).ready(function(){
    var canvas = document.getElementById('Jtopo');//获取画布
    var stage = new JTopo.Stage(canvas);//获取舞台
    //显示工具栏
    //showJTopoToobar(stage);
    var scene = new JTopo.Scene(stage);//把舞台添加到画布
    scene.background = '01.png';//画布背景

    var node = new JTopo.Node("Hello");//新节点 默认test=hello
    node.setLocation(409, 269);//节点位置
    scene.add(node);//将节点添加到场景

    node.mousedown(function(event){//设置鼠标事件
        if(event.button == 2){
            node.text = '按下右键';
        }else if(event.button == 1){
            node.text = '按下中键';
        }else if(event.button == 0){
            node.text = '按下左键';
        }
    });

    node.mouseup(function(event){
        if(event.button == 2){
            node.text = '松开右键';
        }else if(event.button == 1){
            node.text = '松开中键';
        }else if(event.button == 0){
            node.text = '松开左键';
        }
    });
    node.click(function(event){
        console.log("单击");
    });
    node.dbclick(function(event){
        console.log("双击");
    });
    node.mousedrag(function(event){
        console.log("拖拽");
    });
    node.mouseover(function(event){
        console.log("mouseover");
    });
    node.mousemove(function(event){
        console.log("mousemove");
    });
    node.mouseout(function(event){
        console.log("mouseout");
    });

});