package com.example.frontend.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class ExampleController {
    @RequestMapping(value = "/model")
    public String hello(){
        return "model.html";
    }
}
