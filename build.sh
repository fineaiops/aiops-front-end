cd frontend
# 打包项目，生成jar包
mvn package
cp target/frontend-0.0.1-SNAPSHOT.jar docker
cd docker
# 构建镜像
docker build -t darkone0/vaiopsfrontend .
# 运行镜像
#docker run -d --name frontend -p 8080:8080 aiops-front-end
