# AIOPS前端

#### 介绍
AIOPS项目旨在微服务系统生态运维，主要功能包括微服务链路分析、故障诊断、异常报警和根因追踪。

#### 软件架构
软件架构说明


#### 安装教程
##### 方法1 dockerhub下载镜像部署
1. 安装环境：确保服务器已安装docker
2. 服务器拉取最新镜像
```
docker pull darkone0/aiops-front-end
```
3. 运行镜像实例
```
docker run -d --name frontend -p 8080:8080 darkone0/aiops-front-end
```
4. 访问 http://{服务器ip地址}:8080 可以看到前端界面
##### 方法2 源码编译镜像部署
1. 安装环境：确保服务器已安装docker 和 maven 项目源码拷贝到服务器
2. 镜像构建：进入项目源码根目录，运行frontend/build.sh
```
sh frontend/build.sh
```
使用`docker images`查看镜像是否已经包含 aiops_front_end;\
使用`docker ps -a`查看'NAMES'是否已经包含 frontend
3. 访问 http://{服务器ip地址}:8080 可以看到前端界面。如果在本地部署，默认情况下ip地址为localhost或127.0.0.1。

##### 方法选择建议
方法1基于稳定镜像构建，更加稳定简捷，适合于线上服务器部署；\
方法2基于源码构建，适合本地部署和代码调试。

#### 使用说明


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
